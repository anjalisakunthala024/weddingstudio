import logo from './logo.svg';
import './App.css';
import './templatemo_style.css';

function App() {
  return (
      <div id="templatemo_wrapper_outter">

          <div id="templatemo_wrapper_inner">

              <div id="templatemo_header">

                  <div id="site_title">

                      <h1><a href="http://www.templatemo.com" target="_parent">
                          {/*<img src="images/templatemo_logo.png" alt="CSS Templates"/>*/}
                          <span>Wedding Studio</span>
                      </a></h1>

                  </div>

              </div>

              <div id="templatemo_menu">

                  <ul>
                      <li><a href="#" className="current">Home</a></li>
                      <li><a href="http://www.templatemo.com/page/1" target="_parent">CSS Templates</a></li>
                      <li><a href="http://www.flashmo.com/page/1" target="_parent">Flash Files</a></li>
                      <li><a href="http://www.koflash.com" target="_parent">Gallery</a></li>
                      <li><a href="#">Company</a></li>
                      <li><a href="#">Contact Us</a></li>
                  </ul>

              </div>

              <div id="templatemo_content_wrapper">

                  <div id="templatemo_sidebar_wrapper">

                      <div id="templatemo_sidebar_top"></div>
                      <div id="templatemo_sidebar">

                          <h2>Our Services</h2>

                          <ul className="categories_list">
                              <li><a href="#">Lorem ipsum dolor</a></li>
                              <li><a href="#">Phasellus eget lorem</a></li>
                              <li><a href="#">Sed sit amet sem</a></li>
                              <li><a href="#">Cras eget est vel</a></li>
                              <li><a href="#">Quisque in ligula</a></li>
                              <li><a href="#">Donec a massa dui</a></li>
                              <li><a href="#">Aenean facilisis</a></li>
                              <li><a href="#">Etiam vitae consequat</a></li>
                              <li><a href="#">Aliquam sollicitudin</a></li>
                              <li><a href="#">Nunc fermentum</a></li>
                          </ul>

                          <div className="cleaner_h30"></div>

                          <h2>Newsletter</h2>

                          <form action="#" method="get">
                              <label>Please enter your email address to subscribe our newsletter.</label>
                              <input type="text" value="" name="username" size="10" id="input_field" title="usernmae"/>
                              <input type="submit" name="login" value="Subscribe" alt="login" id="submit_btn"
                                     title="Login"/>
                          </form>

                          <div className="cleaner_h10"></div>

                      </div>
                      <div id="templatemo_sidebar_bottom"></div>

                  </div>

                  <div id="templatemo_content">

                      <div className="content_section">

                          <h2>Welcome to our Website</h2>

                          <div className="float_l_image">
                              <img src="images/templatemo_image_01.png" alt="image"/>
                          </div>

                          <p><a href="http://www.templatemo.com" target="_parent">Free CSS Templates</a> are provided
                              by <a href="http://www.templatemo.com" target="_parent">TemplateMo.com</a> for everyone.
                              You can download, modify and apply any template for your own websites. Credits go to <a
                                  href="http://www.publicdomainpictures.net" target="_blank">Public Domain
                                  Pictures</a> and <a href="http://www.vectorvaco.com/">Free Vectors</a>. Validate <a
                                  href="http://validator.w3.org/check?uri=referer">XHTML</a> &amp; <a
                                  href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.</p>

                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet, ipsum bibendum
                              pretium volutpat, diam magna facilisis ante, at lobortis nisl purus ac ipsum. Mauris purus
                              augue, ullamcorper ac pharetra et, varius vitae sapien. Duis sed purus rutrum purus
                              ultrices condimentum.</p>

                      </div>

                      <div className="content_section">

                          <div className="section_w260  float_l margin_r40">

                              <h2>Gallery</h2>

                              <ul className="gallery">
                                  <li><a href="#"><img src="images/templatemo_image_02.jpg" alt="image 1"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_03.jpg" alt="image 2"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_04.jpg" alt="image 3"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_05.jpg" alt="image 4"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_06.jpg" alt="image 5"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_07.jpg" alt="image 6"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_08.jpg" alt="image 7"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_09.jpg" alt="image 8"/></a></li>
                                  <li><a href="#"><img src="images/templatemo_image_10.jpg" alt="image 9"/></a></li>
                              </ul>

                              <div className="cleaner_h20"></div>
                              <div className="button_01"><a href="#">All Photos</a></div>

                          </div>

                          <div className="section_w260 float_l">

                              <h2>Wedding Story</h2>

                              <h3>Jack &amp; Lary</h3>
                              <p>Suspendisse adipiscing, mauris at pretium tincidunt, dui ligula venenatis odio, nec
                                  ultricies sapien felis in libero. Vivamus ut ante. Suspendisse rutrum ipsum est, sed
                                  iaculis.</p>
                              <div className="button_01"><a href="#">Read more</a></div>

                              <div className="cleaner_h20"></div>

                              <h3>John &amp; April</h3>
                              <p>Mauris dictum leo quis lacus venenatis at semper quam laoreet. Donec molestie libero id
                                  arcu laoreet ac consectetur enim blandit. Nulla ut nunc lorem, at porttitor nunc.</p>
                              <div className="button_01"><a href="#">Read more</a></div>

                          </div>

                          <div className="cleaner"></div>

                      </div>

                  </div>

                  <div className="cleaner"></div>

              </div>

              <div id="templatemo_footer">

                  <ul className="footer_menu">
                      <li><a href="#">Home</a></li>
                      <li><a href="http://www.templatemo.com/page/1" target="_parent">CSS Templates</a></li>
                      <li><a href="http://www.flashmo.com/page/1" target="_parent">Flash Files</a></li>
                      <li><a href="http://www.koflash.com" target="_parent">Gallery</a></li>
                      <li><a href="#">Company</a></li>
                      <li className="last_menu"><a href="#">Contact</a></li>
                  </ul>

                  Copyright © 2048 <a href="#">Your Company Name</a> |
                  <a href="http://www.iwebsitetemplate.com" target="_parent">Website Templates</a> by <a
                  href="http://www.templatemo.com" target="_parent">Free CSS Templates</a></div>

          </div>

      </div>

    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
