package com.weddingstudio.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePageController {

    @GetMapping(value = "/home")
    public String getHomePage (){
        return "HELLO WORLD";
    }

}
