package com.weddingstudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeddingstudioApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeddingstudioApplication.class, args);
	}

}
